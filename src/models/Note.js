const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date().toISOString(),
  },
});

const Note = mongoose.model('notes', noteSchema);

module.exports = {
  Note,
};
