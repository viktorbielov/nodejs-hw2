const {User} = require('../models/User');
const {Note} = require('../models/Note');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const loginPage = (req, res) => {
  res.render('index', {
    title: 'Log In',
    isLogged: true,
  });
};

const registerPage = (req, res) => {
  res.render('reg', {
    title: 'Register',
  });
};

const notesPage = async (req, res) => {
  const notes = await Note.find({
    userId: req.user.userId,
  }).lean();
  res.render('notes', {
    title: 'Your notes',
    isNotesActive: true,
    notes,
  });
};

const creatingNotePage = (req, res) => {
  res.render('create', {
    title: 'Create a note',
    isCreateActive: true,
  });
};

const settingsPage = (req, res) => {
  res.render('settings', {
    title: 'Settings',
    isSettingsActive: true,
  });
};

const toggleNoteCompletionById = async (req, res) => {
  const note = await Note.findById(req.body.id);
  note.completed = !req.body.completed;
  await note.save();
  res.redirect('/notes');
};

const deleteNoteById = async (req, res) => {
  await Note.findByIdAndDelete(req.body.id);
  res.redirect('/notes');
};

const addNote = async (req, res) => {
  const note = new Note({
    text: req.body.text,
    userId: req.user.userId,
  });
  await note.save();
  res.redirect('/notes');
};

const authenticateUser = async (req, res, next) => {
  const {username, password} = req.body;
  const existingUser = await User.findOne({username: username});
  if (existingUser) {
    if (await bcrypt.compare(
        String(password),
        String(existingUser.password))) {
      const payload = {
        username: existingUser.username,
        userId: existingUser._id,
      };
      const token = jwt.sign(payload, process.env.JWT_KEY);
      res.status(200).cookie('securityToken', token, {
        httpOnly: true,
      }).redirect('/notes');
    }
  } else {
    res.status(400).redirect('/');
  }
};

const registerUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    const existingUser = await User.findOne({username: username});
    if (!existingUser) {
      const user = new User({
        username: username,
        password: await bcrypt.hash(password, 10),
      });
      const payload = {
        username: user.username,
        userId: user._id,
      };
      const token = jwt.sign(payload, process.env.JWT_KEY);
      await user.save();
      res.status(200).cookie('securityToken', token, {
        httpOnly: true,
      }).redirect('/notes');
    } else {
      res.status(400).redirect('/');
    }
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const changePassword = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);
    const isPasswordCorrect = await bcrypt
        .compare(
            String(req.body.oldpassword),
            String(user.password));
    if (!isPasswordCorrect) {
      res.status(400).redirect('/');
    }
    user.password = await bcrypt.hash(req.body.newpassword, 10);
    await user.save();
    res.status(400).redirect('/');
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const userLogout = async (req, res, next) => {
  res.clearCookie('securityToken').redirect('/');
};

const deleteUser = async (req, res) => {
  try {
    await Note.deleteMany({userId: req.user.userId});
    await User.findByIdAndDelete(req.user.userId);
    res.clearCookie('securityToken').redirect('/');
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

module.exports = {
  loginPage,
  registerPage,
  notesPage,
  creatingNotePage,
  settingsPage,
  addNote,
  toggleNoteCompletionById,
  deleteNoteById,
  authenticateUser,
  registerUser,
  changePassword,
  userLogout,
  deleteUser,
};
