const {User} = require('../models/User');
const bcrypt = require('bcryptjs');

const getUserInfo = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);
    res.status(200).send({
      user: {
        _id: req.user.userId,
        username: user.username,
        createdDate: user.createdAt,
      },
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user.userId);
    res.status(200).send({message: 'Success'});
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const changeUserPassword = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);
    const isPasswordCorrect = await bcrypt
        .compare(
            String(req.body.oldPassword),
            String(user.password));
    if (!isPasswordCorrect) {
      return res.status(400).send({message: 'Incorrect old password'});
    }
    user.password = await bcrypt.hash(req.body.newPassword, 10);
    await user.save();
    res.status(200).send({message: 'Success'});
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
