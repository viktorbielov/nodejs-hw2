const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {User} = require('../models/User');

const createUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    const user = new User({
      username: username,
      password: await bcrypt.hash(password, 10),
    });
    await user.save();
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const loginUser = async (req, res) => {
  const user = await User.findOne({username: req.body.username});
  if (user && await bcrypt.compare(
      String(req.body.password),
      String(user.password))) {
    const payload = {username: user.username, userId: user._id};
    const token = jwt.sign(payload, process.env.JWT_KEY);
    return res.status(200).json({message: 'Success', jwt_token: token});
  }
  return res.status(400).json({message: 'User is not authorized'});
};

module.exports = {
  createUser,
  loginUser,
};
