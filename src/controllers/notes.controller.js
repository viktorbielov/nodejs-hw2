const {Note} = require('../models/Note');

const getNotes = async (req, res) => {
  try {
    const notes = await Note.find({userId: req.user.userId});
    res.status(200).send({
      offset: 0,
      limit: 0,
      count: notes.length,
      notes: notes,
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const addNote = async (req, res) => {
  try {
    const note = new Note({
      text: req.body.text,
      userId: req.user.userId,
      checked: false,
    });
    await note.save();
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const getNoteById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (note.userId !== req.user.userId) {
      res.status(400).send({message: 'Bad request'});
    };
    res.status(200).send({note: note});
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  };
};

const editNoteById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (note.userId !== req.user.userId) {
      res.status(400).send({message: 'Bad request'});
    }
    await Note.findByIdAndUpdate(req.params.id, {text: req.body.text});
    res.status(200).send({message: 'Success'});
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  };
};

const toggleNoteCompletionById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (note.userId !== req.user.userId) {
      res.status(400).send({message: 'Bad request'});
    }
    await Note.findByIdAndUpdate(req.params.id, {completed: !note.completed});
    res.status(200).send({message: 'Success'});
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const deleteNoteById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (note.userId !== req.user.userId) {
      res.status(400).send({message: 'Bad request'});
    }
    await Note.findByIdAndDelete(req.params.id);
    res.status(200).send({message: 'Success'});
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

module.exports = {
  getNotes,
  addNote,
  getNoteById,
  editNoteById,
  toggleNoteCompletionById,
  deleteNoteById,
};
