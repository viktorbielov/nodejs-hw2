const express = require('express');
const {frontendMiddleware} = require('../middleware/frontendMiddleware');
const {
  loginPage,
  registerPage,
  notesPage,
  creatingNotePage,
  settingsPage,
  addNote,
  toggleNoteCompletionById,
  deleteNoteById,
  authenticateUser,
  registerUser,
  changePassword,
  userLogout,
  deleteUser,
} = require('../controllers/render.controller');

const router = new express.Router();

router.get('/', loginPage);
router.post('/', authenticateUser);
router.get('/reg', registerPage);
router.post('/register', registerUser);
router.get('/notes', frontendMiddleware, notesPage);
router.get('/create', frontendMiddleware, creatingNotePage);
router.get('/settings', frontendMiddleware, settingsPage);
router.post('/create', frontendMiddleware, addNote);
router.post('/complete', frontendMiddleware, toggleNoteCompletionById);
router.post('/delete', frontendMiddleware, deleteNoteById);
router.post('/changepassword', frontendMiddleware, changePassword);
router.post('/logout', userLogout);
router.post('/deleteuser', frontendMiddleware, deleteUser);


module.exports = {
  renderRouter: router,
};
