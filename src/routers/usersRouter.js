const express = require('express');
const {
  getUserInfo,
  deleteUser,
  changeUserPassword,
} = require('../controllers/users.controller');

const router = new express.Router();

router.get('/', getUserInfo);
router.delete('/', deleteUser);
router.patch('/', changeUserPassword);

module.exports = {
  usersRouter: router,
};
