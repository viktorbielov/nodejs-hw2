const express = require('express');
const {
  createUser,
  loginUser,
} = require('../controllers/auth.controller');

const router = new express.Router();

router.post('/register', createUser);
router.post('/login', loginUser);

module.exports = {
  authRouter: router,
};
