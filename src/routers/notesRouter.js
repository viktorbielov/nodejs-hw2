const express = require('express');
const {
  getNotes,
  addNote,
  getNoteById,
  editNoteById,
  toggleNoteCompletionById,
  deleteNoteById,
} = require('../controllers/notes.controller');

const router = new express.Router();

router.get('/', getNotes);
router.post('/', addNote);
router.get('/:id', getNoteById);
router.put('/:id', editNoteById);
router.patch('/:id', toggleNoteCompletionById);
router.delete('/:id', deleteNoteById);

module.exports = {notesRouter: router};
