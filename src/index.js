const PORT = 8080;
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const path = require('path');
const app = express();
const ehbs = require('express-handlebars');
const hbs = ehbs.create({defaultLayout: 'main', extname: 'hbs'});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', './src/views');

const {usersRouter} = require('./routers/usersRouter');
const {authRouter} = require('./routers/authRouter');
const {notesRouter} = require('./routers/notesRouter');
const {renderRouter} = require('./routers/renderRouter');
const {authMiddleware} = require('./middleware/authMiddleware');

require('dotenv').config();

app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use('/', renderRouter);
app.use('/api/auth', authRouter);
app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/users/me', authMiddleware, usersRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URI);
    app.listen(PORT, () => console.log(`Server running at port: ${PORT}`));
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

/**
 * Error handler
 * @param {object} err The first number.
 * @param {object} req The second number.
 * @param {object} res sum of the two numbers.
 * @param {object} next sum of the two numbers.
 */
function errorHandler(err, req, res, next) {
  if (err.status === 400 || err.status === 404) {
    res.status(err.status).send({'message': err.message});
  }
  console.error(err);
  res.status(500).send({'message': 'Server error'});
}
